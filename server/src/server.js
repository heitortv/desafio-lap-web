require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')()
const mongoose = require('mongoose')
const app = express()
const router = require('./routes/route')

var mongoUrl = process.env.MONGO_URL || 'mongodb://admin:senha@ds113749.mlab.com:13749/desafio-lap-web' //  Um usuario mlab que eu criei para cadastrar employees
mongoose.connect(mongoUrl)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({'extended': 'false'}))
app.use(cors)
app.use('/api', router)

app.get('*', (req, res) => {
  res.status(404).send('<h1>ERRO 404</h1><p>Rota inexistente.</p>')
})

app.post('*', (req, res) => {
  res.status(404).send('<h1>ERRO 404</h1><p>Rota inexistente.</p>')
})

let server = app.listen(process.env.PORT || 3000, () => {
  let host = server.address().address
  let port = server.address().port
  let family = server.address().family
  console.log('(%s) Rodando no endereço http://%s:%s', family, host, port)
})
