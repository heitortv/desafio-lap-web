var express = require('express')
var router = express.Router()
const Employee = require('../models/employee')
const exemploJson = require('../amostras/exemploFornecido.json')

router.get('/employees', (req, res) => {
  Employee.retornarEmployees((err, employees) => {
    if(err){
      return res.status(400).json(err.errors)
    }
    res.json(employees)
  })
})

router.get('/employeessample', (req, res) => {
    res.status(200).json(exemploJson)
})

router.post('/employees', (req, res) => {
  let employee = req.body
  console.log(employee)
  Employee.adicionarEmployee(employee, (err, employee) => {
    if(err){
      return res.status(400).json(err.errors)
    }
    res.json(employee)
  })
})


module.exports = router