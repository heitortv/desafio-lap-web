const mongoose = require('mongoose')

let EmployeeSchema = new mongoose.Schema({
  nome: {
    type: String,
    required: [true, "É necessário preencher o campo nome."],
    minlength: [3, "O nome deve ter entre 3 e 50 caracteres."],
    maxlength: [50, "O nome deve ter entre 3 e 50 caracteres."]
  },
  sobrenome: {
    type: String,
    required: [true, "É necessário preencher o campo sobrenome."],
    minlength: [3, "O sobrenome deve ter entre 3 e 50 caracteres."],
    maxlength: [50, "O sobrenome deve ter entre 3 e 50 caracteres."]
  },
  participacao: {
    type: Number,
    required: [true, "É necessário preencher o campo participação."],
    min: [1, "O percentual de participação deve estar entre 1 e 100."],
    max: [100, "O percentual de participação deve estar entre 1 e 100."]
  }
})

module.exports = Employee = mongoose.model('Employee', EmployeeSchema)

module.exports.adicionarEmployee = (employee, callback) => {
  Employee.create(employee, callback)
}

module.exports.retornarEmployees = (callback) => {
  Employee.find({}, callback)
}

