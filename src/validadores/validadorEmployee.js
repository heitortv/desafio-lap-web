export default {
  validar (employee) {
    let isNomePreenchido = employee.nome.length >= 3 && employee.nome.length <= 50
    let isSobrenomePreenchido = employee.sobrenome.length >= 3 && employee.nome.length <= 50
    let isParticipacaoPreenchido = !!employee.participacao && employee.participacao >= 1 && employee.participacao <= 100

    return !isNomePreenchido ? 'O nome deve ter entre 3 e 50 caracteres.'
      : !isSobrenomePreenchido ? 'O sobrenome deve ter entre 3 e 50 caracteres.'
        : !isParticipacaoPreenchido ? 'A participaçãp deve ser um número entre 1 e 100.'
          : ''
  }
}