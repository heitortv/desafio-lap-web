import Api from './employeesApi'

export default {

  retornarEmployeesSample () {
    return Api().get('/employeessample')
  },
  retornarEmployees () {
    return Api().get('/employees')
  },
  adicionarEmployee (employee) {
    return Api().post('/employees', employee)
      .then(res => {
        console.log(res)
      })
  }
}
