import { Doughnut, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
  mixins: [reactiveProp],
  extends: Doughnut,
  mounted () {
    console.log(this)
    this.renderChart(this.chartData, {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        position: 'right',
        labels: {
          boxWidth: 14,
          fontSize: 14,
          padding: 20
        }
      }
    })
  }
}
